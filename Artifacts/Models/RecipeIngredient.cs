﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasteringMVC4.Models
{
    public class RecipeIngredient
    {
        public int RecipeIngredientID { get; set; }
        public Ingredient Ingredient { get; set; }
        public double Amount { get; set; }
        public string AmountType { get; set; }
    }
}
