﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasteringMVC4.Models
{
    public class Recipe
    {
        public int RecipeID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Time { get; set; } // In minutes
        public string Image { get; set; }

        public ICollection<RecipeInstruction> RecipeInstructions { get; set; }
        public ICollection<RecipeIngredient> RecipeIngredients { get; set; }

        public Recipe()
        {
            RecipeInstructions = new HashSet<RecipeInstruction>();
            RecipeIngredients = new HashSet<RecipeIngredient>();
        }
    }
}
