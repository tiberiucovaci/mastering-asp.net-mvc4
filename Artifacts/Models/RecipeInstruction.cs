﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasteringMVC4.Models
{
    public class RecipeInstruction
    {
        public int RecipeInstructionID { get; set; }
        public int Order { get; set; }
        public string InstructionText { get; set; }
    }
}
