﻿using MasteringMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasteringMVC.DAL.Mock
{
    public class RecipeRepository : IRecipeRepository
    {
        static IList<Recipe> _recipes = new List<Recipe>() { 
                new Recipe(){
                    RecipeID = 1,
                    Name = "Pizza",
                    Price = 13m,
                    Time = 30,
                    Image = "pizza-clip",
                    RecipeIngredients = new List<RecipeIngredient>(){
                        new RecipeIngredient(){
                            RecipeIngredientID = 1,
                            Amount = 1.0,
                            AmountType = "pcs",
                            Ingredient = new Ingredient(){
                                IngredientID = 1,
                                Name = "Meat",
                                Type = "Meat"
                            }
                        },
                        new RecipeIngredient(){
                            RecipeIngredientID = 2,
                            Amount = 1.0,
                            AmountType = "pcs",
                            Ingredient = new Ingredient(){
                                IngredientID = 1,
                                Name = "Cheese",
                                Type = "Dairy"
                            }
                        }
                    },
                    RecipeInstructions = new List<RecipeInstruction>(){
                        new RecipeInstruction(){
                            InstructionText = "Pick up the phone.",
                            Order = 1,
                            RecipeInstructionID = 1
                        },
                        new RecipeInstruction(){
                            InstructionText = "Dial (555) 1234 555.",
                            Order = 2,
                            RecipeInstructionID = 2
                        },
                        new RecipeInstruction(){
                            InstructionText = "Place your order.",
                            Order = 2,
                            RecipeInstructionID = 3
                        }

                    }
                    
                },
                new Recipe(){
                    RecipeID = 2,
                    Name = "Hamburger",
                    Price = 3m,
                    Time = 1,
                    Image = "hamburger-clip",
                    RecipeIngredients = new List<RecipeIngredient>(){
                        new RecipeIngredient(){
                            RecipeIngredientID = 1,
                            Amount = 1.0,
                            AmountType = "pcs",
                            Ingredient = new Ingredient(){
                                IngredientID = 1,
                                Name = "Meat",
                                Type = "Meat"
                            }
                        }
                    },
                    RecipeInstructions = new List<RecipeInstruction>(){
                        new RecipeInstruction(){
                            InstructionText = "Go buy it from McD",
                            Order = 1,
                            RecipeInstructionID = 4
                        }
                    }
                    
                }

            };

        public IQueryable<Recipe> FetchAll()
        {
            return _recipes.AsQueryable();
        }

        public IQueryable<Recipe> FetchByCriteria(System.Linq.Expressions.Expression<Func<Recipe, bool>> criteria)
        {
            return _recipes.AsQueryable().Where(criteria);
        }

        public Recipe FetchByID(int recipeID)
        {
            return _recipes.SingleOrDefault(r => r.RecipeID == recipeID);
        }

        public void AddRecipe(Recipe recipe)
        {
            _recipes.Add(recipe);
        }

        public void ModifyRecipe(Recipe recipe)
        {
            RemoveRecipe(recipe.RecipeID);
            AddRecipe(recipe);
        }

        public void RemoveRecipe(int recipeID)
        {
            _recipes.Remove(FetchByID(recipeID));
        }
    }
}
