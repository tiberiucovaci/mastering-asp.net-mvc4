﻿using MasteringMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MasteringMVC.Controllers
{
    public class RecipeController : Controller
    {

        IRecipeRepository _repository;
        //
        // GET: /Recipe/

        public RecipeController(IRecipeRepository repository)
        {
            _repository = repository;
        }


        [HandleError(View="Error")]
        public ActionResult Index()
        {
            //throw new Exception();
            return View();
        }



        public ActionResult Top(string searchTerm)
        {
            if (!string.IsNullOrEmpty(searchTerm))
            {
                var model = _repository.FetchByCriteria(r => r.Name.Contains(searchTerm));

                if(Request.IsAjaxRequest()){

                    return PartialView("_SearchResult", model);
                }
                else{
                    return View(model);
                }
            }
            else
            {
                return View(_repository.FetchAll());
            }
        }

        //public ActionResult Search()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Search(string searchTerm)
        //{
        //    if (!Request.IsAjaxRequest())
        //    {

        //    }
        //    return View();
        //}

        //
        // GET: /Recipe/Details/5

        public ActionResult Details(int id)
        {
            var recipe = _repository.FetchByID(id);
            return View(recipe);
        }

        //
        // GET: /Recipe/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Recipe/Create

        [HttpPost]
        public ActionResult Create(Recipe recipe)
        {
            try
            {
                //var recipe = new Recipe();
                //TryUpdateModel<Recipe>(recipe);
                // Read recipe data; 
                // Validate data!
                if (ModelState.IsValid) {
                    _repository.AddRecipe(recipe);
                    return RedirectToAction("Index");
                }

            }
            catch
            {
            }
            return View();
        }

        //
        // GET: /Recipe/Edit/5

        public ActionResult Edit(int id)
        {
            var recipe = _repository.FetchByID(id);
            return View(recipe);
        }

        //
        // POST: /Recipe/Edit/5

        [HttpPost]
        public ActionResult Edit(Recipe recipe)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _repository.ModifyRecipe(recipe);
                    return RedirectToAction("Index");
                }
            }
            catch
            {
            }
            return View();
        }

        //
        // GET: /Recipe/Delete/5

        public ActionResult Delete(int id)
        {
            var recipe = _repository.FetchByID(id);
            return View(recipe);
        }

        //
        // POST: /Recipe/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                _repository.RemoveRecipe(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
