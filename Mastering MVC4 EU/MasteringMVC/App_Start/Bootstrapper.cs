using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using MasteringMVC.Models;
using MasteringMVC.DAL.EF;
using System.Data.Entity;

namespace MasteringMVC
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      Database.SetInitializer<RecipeContext>(new RecipeDbInitializer());
      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
        container.RegisterType<IRecipeRepository, RecipeRepository>();
    
    }
  }
}