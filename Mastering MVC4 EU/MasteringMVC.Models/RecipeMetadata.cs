﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasteringMVC.Models
{
    public class RecipeMetadata
    {
        [Display(Name="Recipe name")]
        [Required(ErrorMessage="{0} is required field")]
        public object Name { get; set; }

        [Range(0, 100)]
        [DataType(DataType.Currency)]
        public object Price { get; set; }

    }

    [MetadataType(typeof(RecipeMetadata))]
    public partial class Recipe
    {

    }
    

}
