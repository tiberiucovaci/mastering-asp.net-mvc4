﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MasteringMVC.Models
{
    public interface IRecipeRepository
    {
        IQueryable<Recipe> FetchAll();
        IQueryable<Recipe> FetchByCriteria(Expression<Func<Recipe, bool>> criteria);
        Recipe FetchByID(int recipeID);

        void AddRecipe(Recipe recipe);
        void ModifyRecipe(Recipe recipe);
        void RemoveRecipe(int recipeID);

    }
}
