﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasteringMVC.Models
{
    public class RecipeInstruction
    {
        public int RecipeInstructionID { get; set; }
        public int Order { get; set; }
        public string InstructionText { get; set; }
    }
}
