﻿using MasteringMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasteringMVC.DAL.EF
{
    public class RecipeDbInitializer : System.Data.Entity.DropCreateDatabaseAlways<RecipeContext>
    {
        protected override void Seed(RecipeContext context)
        {
            context.Ingredients.Add(new Ingredient { IngredientID = 1, Name = "Fish sticks", Type = "Fish" });
            context.Ingredients.Add(new Ingredient { IngredientID = 2, Name = "Rice", Type = "Pasta" });
            context.Ingredients.Add(new Ingredient { IngredientID = 3, Name = "Mayonaise", Type = "Condiment" });
            context.Ingredients.Add(new Ingredient { IngredientID = 4, Name = "Water", Type = "Drink" });
            context.Ingredients.Add(new Ingredient { IngredientID = 5, Name = "Take-Away Menu" });
            context.Ingredients.Add(new Ingredient { IngredientID = 6, Name = "Cereal", Type = "Breakfast" });
            context.Ingredients.Add(new Ingredient { IngredientID = 7, Name = "Milk", Type = "Dairy" });
            context.Ingredients.Add(new Ingredient { IngredientID = 8, Name = "Mashed Potatoes", Type = "Potatoes" });
            context.Ingredients.Add(new Ingredient { IngredientID = 9, Name = "Frozen Meatballs", Type = "Meat" });
            context.Ingredients.Add(new Ingredient { IngredientID = 10, Name = "Wafflemix", Type = "Mix" });
            context.Ingredients.Add(new Ingredient { IngredientID = 11, Name = "Jam", Type = "Condiment" });
            context.Ingredients.Add(new Ingredient { IngredientID = 12, Name = "Minced beef", Type = "Meat" });
            context.Ingredients.Add(new Ingredient { IngredientID = 13, Name = "Hamburger bun", Type = "Bread" });
            context.Ingredients.Add(new Ingredient { IngredientID = 14, Name = "Hamburger dressing", Type = "Condiment" });
            context.Ingredients.Add(new Ingredient { IngredientID = 15, Name = "Tomatoe", Type = "Vegetable" });
            context.Ingredients.Add(new Ingredient { IngredientID = 16, Name = "Onion", Type = "Vegetable" });
            context.Ingredients.Add(new Ingredient { IngredientID = 17, Name = "Ham", Type = "Meat" });
            context.Ingredients.Add(new Ingredient { IngredientID = 18, Name = "Cheeze" });
            context.Ingredients.Add(new Ingredient { IngredientID = 19, Name = "Bread", Type = "Bread" });
            context.Ingredients.Add(new Ingredient { IngredientID = 20, Name = "Ice cream", Type = "Desert" });
            context.Ingredients.Add(new Ingredient { IngredientID = 21, Name = "Chocolate sauce", Type = "Condiment" });
            context.Ingredients.Add(new Ingredient { IngredientID = 22, Name = "Merenge", Type = "Desert" });
            context.Ingredients.Add(new Ingredient { IngredientID = 23, Name = "Whipped cream", Type = "Dairy" });
            context.Ingredients.Add(new Ingredient { IngredientID = 24, Name = "Banana", Type = "Fruit" });
            context.Ingredients.Add(new Ingredient { IngredientID = 25, Name = "Penne", Type = "Pasta" });
            context.Ingredients.Add(new Ingredient { IngredientID = 26, Name = "Meat sauce", Type = "Meat" });

            var recipe = new Recipe { RecipeID = 1, Name = "Fish sticks and Rice", Price = 2, Time = 20, Image = "fishsticks-mine" };

            var instruction = new RecipeInstruction() { InstructionText = "Follow instructions on rice package to make rice", Order = 0 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Fry fish sticks", Order = 1 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Plate and serve with mayo", Order = 2 };
            recipe.RecipeInstructions.Add(instruction);

            var ingredient = context.Ingredients.Find(1);
            var recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 2, AmountType = "Portions of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(2);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 2, AmountType = "Portions of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(3);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(4);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient };
            recipe.RecipeIngredients.Add(recipeIngredient);

            context.Recipes.Add(recipe);

            recipe = new Recipe { RecipeID = 2, Name = "Pizza", Price = 13, Time = 30, Image = "pizza-clip" };

            instruction = new RecipeInstruction() { InstructionText = "Find number for local Pizza take-out", Order = 0 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Call and order pizza", Order = 1 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Pick up pizza", Order = 2 };
            recipe.RecipeInstructions.Add(instruction);

            ingredient = context.Ingredients.Find(5);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 1 };
            recipe.RecipeIngredients.Add(recipeIngredient);

            context.Recipes.Add(recipe);

            recipe = new Recipe { RecipeID = 3, Name = "Pasta with Meatsauce", Price = 3, Time = 11, Image = "pasta-clip" };

            instruction = new RecipeInstruction() { InstructionText = "Cook pasta according to instructions on package", Order = 0 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Heat meatsauce", Order = 1 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Plate and serve", Order = 2 };
            recipe.RecipeInstructions.Add(instruction);

            ingredient = context.Ingredients.Find(25);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 2, AmountType = "Portions of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(26);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 1, AmountType = "Package of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            context.Recipes.Add(recipe);

            recipe = new Recipe { RecipeID = 4, Name = "Cereal with Millk", Price = 1, Time = 1, Image = "cereal-clip" };

            instruction = new RecipeInstruction() { InstructionText = "Pour desired amount of cereal in a bowl", Order = 0 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Top off with milk", Order = 1 };
            recipe.RecipeInstructions.Add(instruction);

            ingredient = context.Ingredients.Find(6);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 1, AmountType = "Portions of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(7);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 3, AmountType = "DL" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            context.Recipes.Add(recipe);

            recipe = new Recipe { RecipeID = 5, Name = "Meatballs and Mashed Potatoes", Price = 4, Time = 5, Image = "meatballs-clip" };

            instruction = new RecipeInstruction() { InstructionText = "Cook mashed potatoes according to instructions on package", Order = 0 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Heat meatballs in microwave", Order = 1 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Plate and serve", Order = 2 };
            recipe.RecipeInstructions.Add(instruction);

            ingredient = context.Ingredients.Find(8);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 2, AmountType = "Portions of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(9);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 2, AmountType = "Portions of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            context.Recipes.Add(recipe);

            recipe = new Recipe { RecipeID = 6, Name = "Waffles and Jam", Price = 3, Time = 12, Image = "waffles-clip" };

            instruction = new RecipeInstruction() { InstructionText = "Mix wafflemix with water", Order = 0 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Heat waffl iron and add butter if needed", Order = 1 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Pour waffle mixture into waffle iron and wait 2 minutes", Order = 2 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Serve with Jam", Order = 3 };
            recipe.RecipeInstructions.Add(instruction);

            ingredient = context.Ingredients.Find(4);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(10);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 1, AmountType = "Package of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(11);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 3, AmountType = "Tblsp" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            context.Recipes.Add(recipe);

            recipe = new Recipe { RecipeID = 7, Name = "Hamburgers", Price = 3, Time = 15, Image = "hamburger-clip" };

            instruction = new RecipeInstruction() { InstructionText = "Divide minced meat into 100g patties", Order = 0 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Fry patties on both sides or grill them on a table grill", Order = 1 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Slice tomatoes and onions", Order = 2 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Serve with hamburger buns and dressing to taste", Order = 3 };
            recipe.RecipeInstructions.Add(instruction);

            ingredient = context.Ingredients.Find(12);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 400, AmountType = "g" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(13);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 4 };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(14);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(15);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 1 };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(16);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 1 };
            recipe.RecipeIngredients.Add(recipeIngredient);

            context.Recipes.Add(recipe);

            recipe = new Recipe { RecipeID = 8, Name = "Ham and Cheeze Sandwich", Price = 2, Time = 5, Image = "sandwich-clip" };

            instruction = new RecipeInstruction() { InstructionText = "Put ham and cheeze on bread", Order = 0 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Add a 2nd slice of bread on top", Order = 1 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Grill sandwich in a sandwich grill", Order = 2 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Serve with mustard and hot sauce", Order = 3 };
            recipe.RecipeInstructions.Add(instruction);

            ingredient = context.Ingredients.Find(17);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 1, AmountType = "Slice of " };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(18);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 1, AmountType = "Slice of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(19);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 2, AmountType = "Slices of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            context.Recipes.Add(recipe);

            recipe = new Recipe { RecipeID = 9, Name = "Magic Ice cream", Price = 4, Time = 3, Image = "icecream-clip" };

            instruction = new RecipeInstruction() { InstructionText = "Slice bananas", Order = 0 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Mix all ingredients in a serving bowl", Order = 1 };
            recipe.RecipeInstructions.Add(instruction);

            instruction = new RecipeInstruction() { InstructionText = "Pour chocolate sauce on top", Order = 2 };
            recipe.RecipeInstructions.Add(instruction);

            ingredient = context.Ingredients.Find(20);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 2, AmountType = "Scoops of" };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(21);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(22);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 5 };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(23);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient };
            recipe.RecipeIngredients.Add(recipeIngredient);

            ingredient = context.Ingredients.Find(24);
            recipeIngredient = new RecipeIngredient() { Ingredient = ingredient, Amount = 1 };
            recipe.RecipeIngredients.Add(recipeIngredient);

            context.Recipes.Add(recipe);

            context.SaveChanges();

        }
    }
}