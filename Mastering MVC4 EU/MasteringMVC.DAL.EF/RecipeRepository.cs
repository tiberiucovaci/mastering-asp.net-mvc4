﻿using MasteringMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasteringMVC.DAL.EF
{
    public class RecipeRepository : IRecipeRepository
    {
        private RecipeContext db = new RecipeContext();

        public IQueryable<Recipe> FetchAll()
        {
            return db.Recipes;
        }

        public IQueryable<Recipe> FetchByCriteria(System.Linq.Expressions.Expression<Func<Recipe, bool>> criteria)
        {
            return db.Recipes.Where(criteria);
        }

        public Recipe FetchByID(int recipeID)
        {
            return db.Recipes.Find(recipeID);
        }

        public void AddRecipe(Recipe recipe)
        {
            db.Recipes.Add(recipe);
            db.SaveChanges();
        }

        public void ModifyRecipe(Recipe recipe)
        {
            db.Entry(recipe).State = System.Data.EntityState.Modified;
            db.SaveChanges();
        }

        public void RemoveRecipe(int recipeID)
        {
            Recipe recipe = db.Recipes.Find(recipeID);
            db.Recipes.Remove(recipe);
            db.SaveChanges();
        }
    }
}
